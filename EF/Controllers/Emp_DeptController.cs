﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EF.Models;

namespace EF.Controllers
{
    public class Emp_DeptController : Controller
    {
        private employeesEntities1 db = new employeesEntities1();

        // GET: Emp_Dept
        public ActionResult Index()
        {
            return View(db.Emp_Dept.ToList());
        }

        // GET: Emp_Dept/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Emp_Dept emp_Dept = db.Emp_Dept.Find(id);
            if (emp_Dept == null)
            {
                return HttpNotFound();
            }
            return View(emp_Dept);
        }

        // GET: Emp_Dept/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Emp_Dept/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Emp_ID,Dept_ID,From_Date,To_Date")] Emp_Dept emp_Dept)
        {
            if (ModelState.IsValid)
            {
                db.Emp_Dept.Add(emp_Dept);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(emp_Dept);
        }

        // GET: Emp_Dept/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Emp_Dept emp_Dept = db.Emp_Dept.Find(id);
            if (emp_Dept == null)
            {
                return HttpNotFound();
            }
            return View(emp_Dept);
        }

        // POST: Emp_Dept/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Emp_ID,Dept_ID,From_Date,To_Date")] Emp_Dept emp_Dept)
        {
            if (ModelState.IsValid)
            {
                db.Entry(emp_Dept).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(emp_Dept);
        }

        // GET: Emp_Dept/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Emp_Dept emp_Dept = db.Emp_Dept.Find(id);
            if (emp_Dept == null)
            {
                return HttpNotFound();
            }
            return View(emp_Dept);
        }

        // POST: Emp_Dept/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Emp_Dept emp_Dept = db.Emp_Dept.Find(id);
            db.Emp_Dept.Remove(emp_Dept);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
